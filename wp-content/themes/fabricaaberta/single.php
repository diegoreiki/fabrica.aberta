<?php
    $home = get_template_directory_uri();
    get_header();
?>

<div class="container">
    <?php
        $args = array(
            'post_type' => 'post'
        );
        if (have_posts()) {
            while (have_posts()) {
                the_post(); ?>

                <div class="row">
                    <div class="col-md-12">
                        <div class="title-section-blog">
                            <h1 class="title text-title"><?php the_title(); ?></h1>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
    ?>

    <div class="box-content-blog">

        <div class="row">
            <div class="col-md-9">
                <?php
                $args = array(
                    'post_type' => 'post'
                );
                if (have_posts()) {
                    while (have_posts()) {
                        the_post(); ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="box-post clearfix">
                                    <div class="box-image">
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="w-100" />
                                    </div>
                                    <div class="box-content">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                }
            }
            ?>
            </div>
            <div class="col-md-3">
                <div class="box-aside-blog">
                    <?php if (dynamic_sidebar('widget_aside_blog')) : else : endif; ?>
                </div>
            </div>
        </div>

        <div class="box-comment">
            <div class="row">
                <div class="col-md-12">
                    <h2>Deixe seu comentário</h2>
                    <?php
                    $args = array(
                        'post_type' => 'post'
                    );
                    if (have_posts()) {
                        while (have_posts()) {
                            the_post();

                            if (comments_open() || get_comments_number()) :
                                comments_template();
                            endif;
                        }
                    }
                    ?>
                </div>
            </div>
        </div>

    </div>
</div>

<?php get_footer(); ?>