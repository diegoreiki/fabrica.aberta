<?php /* Template Name: Home */ ?>

<?php
    $home = get_template_directory_uri();
    get_header();
?>

<div class="box-fullbanner">
    <div class="content">
        <?php
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
                    the_content();
                }
            }
        ?>   
    </div>

    <div class="content mobile">
        <div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
            <?php the_post_thumbnail(); ?>
        </div>

        <div class="ms-slide-info ms-dir-h ms-align-bottom" style="margin-top: 10px; position: relative; min-height: 100px;">
            <div class="ms-info" style="opacity: 1;">
                <div class="container box-title">
                    <h2>O programa Fábrica <br class="visible-md visible-lg">Aberta cresceu!</h2>
                    <p>Conheça também as plantas de Santo André<br>e Bahía Blanca na Argentina</p>
                    <a href="http://srv252.teste.website/~fabricaabertacom/agende-sua-visita/">AGENDE AGORA!</a>
                </div>
            </div>
        </div>    

        <span class="ico-arrow d-none fadeOutDown animated infinite">
            <img src="<?= $home ?>/assets/img/ico-arrow-down.png" class="img-fluid" />
        </span>
    </div>
</div>

<div class="section section-features">
    <div class="container">
        
        <div class="section-title animated">
            <h3 class="title text-title"><?= get_field('titulo'); ?></h3>                    
            <p class="support text-support"><?= get_field('descricao'); ?></p>
        </div>

        <div class="list-features wow fadeInRight animated">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('pilares_imagem_programa'); ?>" alt="<?= get_field('pilares_titulo_programa'); ?>" class="animated infinite" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('pilares_titulo_programa'); ?></strong>
                        <p>
                            <?= get_field('pilares_descricao_programa'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('pilares_imagem_historia'); ?>" alt="<?= get_field('pilares_titulo_historia'); ?>" class="animated infinite" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('pilares_titulo_historia'); ?></strong>
                        <p>
                            <?= get_field('pilares_descricao_historia'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-image">
                    <img src="<?= get_field('pilares_imagem_unipar'); ?>" alt="<?= get_field('pilares_titulo_unipar'); ?>" class="animated infinite" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('pilares_titulo_unipar'); ?></strong>
                        <p>
                            <?= get_field('pilares_descricao_unipar'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section section-about">

    <div class="description-about">
        <div class="section-title wow fadeInLeft animated">
            <h3 class="title"><?= get_field('titulo_visita'); ?></h3>
            <p class="support">
                <?= get_field('descricao_visita'); ?>
            </p>
        </div>
    </div>

    <div class="container">
        <div class="list-statistic wow fadeInRight animated">
            <div class="row">
                <div class="col-md-4">
                    <strong class="count"><?= get_field('visitas_numero_visita_cubatao'); ?></strong>
                    <span class="info"><?= get_field('visitas_fabrica_cubatao'); ?></span>
                    <span class="since"><?= get_field('visitas_fabrica_cubatao'); ?></span>
                </div>
                <div class="col-md-4">
                    <strong class="count"><?= get_field('visitas_numero_visita_santo_andre'); ?></strong>
                    <span class="info"><?= get_field('visitas_fabrica_cubatao'); ?></span>
                    <span class="since"><?= get_field('visitas_data_santo_andre'); ?></span>
                </div>
                <div class="col-md-4">
                    <strong class="count"><?= get_field('visitas_numero_bahia_blanca'); ?></strong>
                    <span class="info"><?= get_field('visitas_fabrica_bahia_blanca'); ?></span>
                    <span class="since"><?= get_field('visitas_data_bahia_blanca'); ?></span>
                </div>                                
            </div>
        </div>

        <div class="box-buttons wow fadeInDown animated">
            <a href="<?= site_url() ?>/agende-sua-visita/" class="btn-custom btn-custom-secondary">agende sua visita agora</a>
        </div>
    </div>
</div>

<?php get_footer(); ?>