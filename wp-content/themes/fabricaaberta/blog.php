<?php /* Template Name: Blog */ ?>

<?php
$home = get_template_directory_uri();
get_header();
?>

<!-- Banner -->
<div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
    <?php the_post_thumbnail(); ?>
</div>
<!-- /Banner -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="title-section-blog">
                <h1 class="title text-title">BLOG FÁBRICA ABERTA</h1>
            </div>
        </div>
    </div>

    <div class="box-content-blog">
        <div class="row">
            <div class="col-md-9">
                <?php
                $args = array(
                    'post_type' => 'post'
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) {
                    while ($loop->have_posts()) {
                        $loop->the_post(); ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="box-post clearfix">

                                    <div class="box-image">
                                        <div class="overflow-image">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" class="w-100" />
                                            </a>
                                        </div>
                                    </div>

                                    <div class="box-title">
                                        <div class="box-date">
                                            <?php 
                                                $data = get_the_date('d-F'); 
                                                $diaMes = explode('-', $data);
                                            ?>
                                            <strong class="day"><?= $diaMes[0]; ?></strong>
                                            <span class="month"><?= $diaMes[1]; ?></span>
                                        </div>
                                        <div class="title-post">
                                            <h2>
                                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                    <?php the_title(); ?>
                                                </a>
                                            </h2>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <p>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <?php the_content(); ?>
                                            </a>
                                        </p>
                                        <a href="<?php the_permalink(); ?>" title="Leia mais" class="link-read-more">Leia mais <i class="fa fa-caret-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                }
            }
            ?>
                <?php wordpress_pagination(); ?>
            </div>
            <div class="col-md-3">
                <div class="box-aside-blog">
                    <?php if (dynamic_sidebar('widget_aside_blog')) : else : endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>