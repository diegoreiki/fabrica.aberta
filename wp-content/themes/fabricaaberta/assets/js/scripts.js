$(function () {
    new WOW().init();

    $(language);

    if ($("body").hasClass("page-template-unipar")) {
        $(factoryOdd);
        $(galleryFactory);
    }

    if ($("body").hasClass("page-template-team")) {
        $(viewMoreTeam);
        $(firstVideo);
    }

    if ($("body").hasClass("page-template-schedule")) {
        $(accordionSchedule);
        $(showFormSchedule);
        $(checkHash);
    }
});

function factoryOdd() {
    $(".content-factory > div:odd").addClass("odd");
}

function galleryFactory() {
    $(".box-gallery img").click(function (e) {
        e.preventDefault();

        var parent = $(this).parents(".tab-pane");
        var image = $(".box-image img", parent).attr("src");
        var imageTarget = $(this).attr("src");

        $(this).attr("src", image);
        $(".box-details .box-image img", parent).attr("src", imageTarget).fadeIn();
    });
}

function viewMoreTeam() {
    $(".view-more").click(function (e) {
        e.preventDefault();

        var parent = $(this).parent();
        var parents = $(this).parents(".box-videos");
        
        $(this).css("visibility", "hidden");

        if (parent.next().length == 1) {
            parent.next().fadeIn();
        } else {
            parents.siblings(".box-videos").fadeIn();
        }
    });
}

function firstVideo() {
    $(".box-videos").each(function (i) {
        if (i == 0) {
            $(this).css("display", "block");
        }
        $(".list-videos .row > div:eq(0)", this).css("display", "block");
    });
}

function accordionSchedule() {

    $(".box-warning strong.mobile").click(function () {
        var status = $(".fa", this);
        console.log(status);

        if (status.hasClass("fa-sort-down")) {
            status.removeClass().addClass("fa fa-sort-up");
            $(this).parent().children("ul").stop().slideDown();
        } else {
            status.removeClass().addClass("fa fa-sort-down");
            $(this).parent().children("ul").stop().slideUp();
        }
    });
}

function showFormSchedule() {
    $(".header-city a").click(function () {
        $(this).parents(".header-city").siblings(".content-city").fadeIn();
    })
}

function checkHash() {

}

function language(){
    $(".box-language a").click(function(e){
        e.preventDefault();
    
        if (!$(this).siblings(".options-language").hasClass("active")){
            $(".options-language").addClass("active");
            $(".options-language").fadeIn();
        } else {
            $(".options-language").removeClass("active");
            $(".options-language").fadeOut();
        }
    });    
}