<?php /* Template Name: A Unipar */ ?>

<?php
$home = get_template_directory_uri();
get_header();
?>

<!-- Banner -->
<div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
    <?php the_post_thumbnail(); ?>
</div>
<!-- /Banner -->

<!-- Section -->
<div class="section section-features">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title animated">
                    <h3 class="title text-title"><?= get_field('titulo'); ?></h3>
                    <p class="support text-support">
                        <?= get_field('descricao'); ?>
                    </p>
                </div>
            </div>
        </div>

        <div class="list-features wow fadeInRight animated">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('missao_imagem'); ?>" alt="<?= get_field('missao_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('missao_titulo'); ?></strong>
                        <p><?= get_field('missao_descricao'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('visao_imagem'); ?>" alt="<?= get_field('visao_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('visao_titulo'); ?></strong>
                        <p><?= get_field('visao_descricao'); ?></p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('visao_imagem'); ?>" alt="<?= get_field('visao_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('visao_titulo'); ?></strong>
                        <p><?= get_field('visao_descricao'); ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-features mobile d-none wow fadeInRight animated animated carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="box-image">
                        <img src="<?= get_field('missao_imagem'); ?>" alt="<?= get_field('missao_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('missao_titulo'); ?></strong>
                        <p><?= get_field('missao_descricao'); ?></p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="box-image">
                        <img src="<?= get_field('visao_imagem'); ?>" alt="<?= get_field('visao_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('visao_titulo'); ?></strong>
                        <p><?= get_field('visao_descricao'); ?></p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="box-image">
                        <img src="<?= get_field('valores_imagem'); ?>" alt="<?= get_field('valores_titulo'); ?>" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('valores_titulo'); ?></strong>
                        <p><?= get_field('valores_descricao'); ?></p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-factory">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title wow fadeInLeft animated">
                    <h3 class="title text-title">nossas fábricas</h3>
                    <p class="support text-support">
                        <?= get_field('nossas_fabricas'); ?>
                    </p>
                </div>
            </div>
        </div>

        <ul class="nav header-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" role="tab" aria-selected="true" href="#factory01">
                    <?= get_field('fabrica_01_titulo'); ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#factory02">
                    <?= get_field('fabrica_02_titulo'); ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#factory03">
                    <?= get_field('fabrica_03_titulo'); ?>
                </a>
            </li>
        </ul>

        <div class="tab-content content-factory wow fadeIn animated" id="myTabContent">

            <div class="tab-pane fade show active" id="factory01" role="tabpanel">
                <div class="box-details">
                    <div class="box-image">
                        <img src="<?= get_field('fabrica_01_imagem_01'); ?>" alt="" class="w-100" />
                    </div>
                    <div class="box-info">
                        <h2><?= get_field('fabrica_01_titulo'); ?></h2>
                        <p>
                            <?= get_field('fabrica_01_descricao'); ?>
                        </p>
                        <a href="<?= get_home_url(); ?><?= get_field('fabrica_01_link'); ?>" class="btn-custom btn-custom-primary">Visite nossa unidade em <?= get_field('fabrica_01_titulo'); ?></a>
                    </div>
                </div>

                <div class="box-gallery">
                    <ul>
                        <li>
                            <img src="<?= get_field('fabrica_01_imagem_02'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_01_imagem_03'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_01_imagem_04'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_01_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>

                <div class="box-gallery mobile d-none carousel slide" data-ride="carousel">
                    <ul class="carousel-inner">
                        <li class="carousel-item active">
                            <img src="<?= get_field('fabrica_01_imagem_02'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_01_imagem_03'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_01_imagem_04'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_01_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-pane fade" id="factory02" role="tabpanel">
                <div class="box-details">
                    <div class="box-image">
                        <img src="<?= get_field('fabrica_02_imagem_01'); ?>" alt="" class="w-100" />
                    </div>
                    <div class="box-info">
                        <h2><?= get_field('fabrica_02_titulo'); ?></h2>
                        <p>
                            <?= get_field('fabrica_02_descricao'); ?>
                        </p>
                        <a href="<?= get_home_url(); ?><?= get_field('fabrica_02_link'); ?>" class="btn-custom btn-custom-primary">Visite nossa unidade em <?= get_field('fabrica_02_titulo'); ?></a>
                    </div>
                </div>

                <div class="box-gallery">
                    <ul>
                        <li>
                            <img src="<?= get_field('fabrica_02_imagem_02'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_02_imagem_03'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_02_imagem_04'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_02_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>

                <div class="box-gallery mobile d-none carousel slide" data-ride="carousel">
                    <ul class="carousel-inner">
                        <li class="carousel-item active">
                            <img src="<?= get_field('fabrica_02_imagem_02'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_02_imagem_03'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_02_imagem_04'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_02_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>
            </div>

            <div class="tab-pane fade" id="factory03" role="tabpanel">
                <div class="box-details">
                    <div class="box-image">
                        <img src="<?= get_field('fabrica_03_imagem_01'); ?>" alt="" class="w-100" />
                    </div>
                    <div class="box-info">
                        <h2><?= get_field('fabrica_03_titulo'); ?></h2>
                        <p>
                            <?= get_field('fabrica_03_descricao'); ?>
                        </p>
                        <a href="<?= get_home_url(); ?><?= get_field('fabrica_03_link'); ?>" class="btn-custom btn-custom-primary">Visite nossa unidade em <?= get_field('fabrica_03_titulo'); ?></a>
                    </div>
                </div>

                <div class="box-gallery">
                    <ul>
                        <li>
                            <img src="<?= get_field('fabrica_03_imagem_02'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_03_imagem_03'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_03_imagem_04'); ?>" alt="" />
                        </li>
                        <li>
                            <img src="<?= get_field('fabrica_03_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>

                <div class="box-gallery mobile d-none carousel slide" data-ride="carousel">
                    <ul class="carousel-inner">
                        <li class="carousel-item active">
                            <img src="<?= get_field('fabrica_03_imagem_02'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_03_imagem_03'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_03_imagem_04'); ?>" alt="" />
                        </li>
                        <li class="carousel-item">
                            <img src="<?= get_field('fabrica_03_imagem_05'); ?>" alt="" />
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-products wow fadeInUp animated">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title wow fadeInLeft animated">
                    <h3 class="title text-title">PRODUTOS ESSENCIAIS PARA O DIA A DIA</h3>
                    <p class="support text-support">
                        <?= get_field('produtos_descricao'); ?>
                    </p>
                </div>
            </div>
        </div>

		<?php 
            $args = array(
                'post_type' => 'products'
            );

            $loop = new WP_Query($args);		
			if ($loop->have_posts()){ ?>
                <div class="box-products">
                    <div class="row">
                    <?php 
                        while ($loop->have_posts()) { 
                            $loop->the_post(); ?>        
                            <div class="col-md-6">
                                <div class="box-image">
                                    <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                </div>
                                <div class="box-info">
                                    <h3><?php the_title(); ?></h3>
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        <?php 
                        }
                        ?> 
                    </div>
                </div>
                <?php 
			} 
		?>	        
    </div>
</div>
<!-- /Section -->

<?php get_footer(); ?>