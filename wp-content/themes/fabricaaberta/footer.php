	<?php $home = get_template_directory_uri(); ?>
	<?php wp_footer(); ?>
	
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="box-logotipo">
                            <img src="<?= $home ?>/assets/img/unipar.png" alt="" class="img-fluid">
                        </div>
                    </div>
                    <div class="col-6">
                        <ul class="box-social-media">
                            <li class="ico-linkedin"><a href="http://www.linkedin.com" target="_blank">Linkedin</a></li>
                            <li class="ico-facebook"><a href="http://www.facebook.com" target="_blank">Facebook</a></li>
                            <li class="ico-instagram"><a href="http://www.instagram.com" target="_blank">Instagram</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <?php
                            $args = array(
                                'theme_location' => 'header_menu'
                            );
                            wp_nav_menu($args);
                        ?>                            
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="copyright">
                            <p>Copyright © <?= get_the_date('Y'); ?> - <?php bloginfo('name'); ?></p>
                        </div>                           
                    </div>
                </div>                
            </div>
        </footer>
	</body>
</html>