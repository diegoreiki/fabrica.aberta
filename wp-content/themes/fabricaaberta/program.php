<?php /* Template Name: O Programa */ ?>

<?php
$home = get_template_directory_uri();
get_header();
?>

<!-- Banner -->
<div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
    <?php the_post_thumbnail(); ?>
</div>
<!-- /Banner -->

<!-- Section -->
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title animated">
                    <h3 class="title text-title"><?= get_field('descricao_titulo'); ?></h3>
                    <div class="container-inner">
                        <p>
                            <strong style="color: #00af5d; font-size: 20px;"><?= get_field('descricao_sub_titulo'); ?></strong>
                        </p>
                        <p class="support text-support">
                            <strong style="color: #6b8b7c; font-weight: 600;"><?= get_field('descricao_chamada'); ?></strong><br />
                            <?= get_field('descricao_conteudo'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-resume">
    <div class="box-banner-bg">
        <img src="<?= $home ?>/assets/img/bg-program.jpg" />
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <strong class="title wow fadeInLeft animated">O FÁBRICA ABERTA CRESCEU!</strong>
            </div>
            <div class="col-md-2">
                <span class="separate"></span>
            </div>
            <div class="col-md-5">
                <div class="box-resume wow fadeInRight animated">
                    <?= get_field('fabrica_cresceu'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-features">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title wow fadeInLeft animated">
                    <h3 class="title">O FÁBRICA ABERTA É...</h3>
                    <div class="container-inner">
                        <p class="support">
                            <?= get_field('o_fabrica_aberta_e'); ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box-features wow fadeIn animated">

                    <ul class="nav header-features" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" role="tab" aria-selected="true" href="#transparent">
                                <img src="<?= get_field('transparencia_imagem'); ?>" alt="<?= get_field('transparencia_titulo'); ?>" class="img-inactive animated infinite">
                                <img src="<?= get_field('transparencia_imagem_ativa'); ?>" alt="<?= get_field('transparencia_titulo'); ?>" class="img-active animated infinite">
                                <strong><?= get_field('transparencia_titulo'); ?></strong>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#community">
                                <img src="<?= get_field('comunidade_imagem'); ?>" alt="<?= get_field('comunidade_titulo'); ?>" class="img-inactive animated infinite">
                                <img src="<?= get_field('comunidade_imagem_ativa'); ?>" alt="<?= get_field('comunidade_titulo'); ?>" class="img-active animated infinite">
                                <strong><?= get_field('comunidade_titulo'); ?></strong>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#education">
                                <img src="<?= get_field('educacao_imagem'); ?>" alt="<?= get_field('educacao_titulo'); ?>" class="img-inactive animated infinite">
                                <img src="<?= get_field('educacao_imagem_ativa'); ?>" alt="<?= get_field('educacao_titulo'); ?>" class="img-active animated infinite">
                                <strong><?= get_field('educacao_titulo'); ?></strong>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#ambient">
                                <img src="<?= get_field('ambiente_imagem'); ?>" alt="<?= get_field('ambiente_titulo'); ?>" class="img-inactive animated infinite">
                                <img src="<?= get_field('ambiente_imagem_ativa'); ?>" alt="<?= get_field('ambiente_titulo'); ?>" class="img-active animated infinite">
                                <strong><?= get_field('ambiente_titulo'); ?></strong>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#security">
                                <img src="<?= get_field('seguranca_imagem'); ?>" alt="<?= get_field('seguranca_titulo'); ?>" class="img-inactive animated infinite">
                                <img src="<?= get_field('seguranca_imagem_ativa'); ?>" alt="<?= get_field('seguranca_titulo'); ?>" class="img-active animated infinite">
                                <strong><?= get_field('seguranca_titulo'); ?></strong>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content content-features" id="myTabContent">
                        <div class="tab-pane fade show active" id="transparent" role="tabpanel">
                            <strong><?= get_field('transparencia_titulo'); ?></strong>
                            <p><?= get_field('transparencia_descricao'); ?></p>
                        </div>
                        <div class="tab-pane fade" id="community" role="tabpanel">
                            <strong><?= get_field('comunidade_titulo'); ?></strong>
                            <p><?= get_field('comunidade_descricao'); ?></p>
                        </div>
                        <div class="tab-pane fade" id="education" role="tabpanel">
                            <strong><?= get_field('educacao_titulo'); ?></strong>
                            <p><?= get_field('educacao_descricao'); ?></p>
                        </div>
                        <div class="tab-pane fade" id="ambient" role="tabpanel">
                            <strong><?= get_field('ambiente_titulo'); ?></strong>
                            <p><?= get_field('ambiente_descricao'); ?></p>
                        </div>
                        <div class="tab-pane fade" id="security" role="tabpanel">
                            <strong><?= get_field('seguranca_titulo'); ?></strong>
                            <p><?= get_field('seguranca_descricao'); ?></p>
                        </div>
                    </div>

                    <div class="carousel slide mobile" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner header-features" id="myTab" role="tablist">

                            <div class="carousel-item nav-item active">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="true" href="#transparent">
                                    <img src="<?= get_field('transparencia_imagem'); ?>" alt="<?= get_field('transparencia_titulo'); ?>" class="img-inactive animated infinite">
                                    <img src="<?= get_field('transparencia_imagem_ativa'); ?>" alt="<?= get_field('transparencia_titulo'); ?>" class="img-active animated infinite">
                                    <strong><?= get_field('transparencia_titulo'); ?></strong>
                                </a>

                                <div class="content-features">
                                    <div class="tab-pane fade show active" id="transparent" role="tabpanel">
                                        <strong><?= get_field('transparencia_titulo'); ?></strong>
                                        <p><?= get_field('transparencia_descricao'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#community">
                                    <img src="<?= get_field('comunidade_imagem'); ?>" alt="<?= get_field('comunidade_titulo'); ?>" class="img-inactive animated infinite">
                                    <img src="<?= get_field('comunidade_imagem_ativa'); ?>" alt="<?= get_field('comunidade_titulo'); ?>" class="img-active animated infinite">
                                    <strong><?= get_field('comunidade_titulo'); ?></strong>
                                </a>

                                <div class="content-features">
                                    <div class="tab-pane fade show active" id="community" role="tabpanel">
                                        <strong><?= get_field('comunidade_titulo'); ?></strong>
                                        <p><?= get_field('comunidade_descricao'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#education">
                                    <img src="<?= get_field('educacao_imagem'); ?>" alt="<?= get_field('educacao_titulo'); ?>" class="img-inactive animated infinite">
                                    <img src="<?= get_field('educacao_imagem_ativa'); ?>" alt="<?= get_field('educacao_titulo'); ?>" class="img-active animated infinite">
                                    <strong><?= get_field('educacao_titulo'); ?></strong>
                                </a>

                                <div class="content-features">
                                    <div class="tab-pane fade show active" id="education" role="tabpanel">
                                        <strong><?= get_field('educacao_titulo'); ?></strong>
                                        <p><?= get_field('educacao_descricao'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#ambient">
                                    <img src="<?= get_field('ambiente_imagem'); ?>" alt="<?= get_field('ambiente_titulo'); ?>" class="img-inactive animated infinite">
                                    <img src="<?= get_field('ambiente_imagem_ativa'); ?>" alt="<?= get_field('ambiente_titulo'); ?>" class="img-active animated infinite">
                                    <strong><?= get_field('ambiente_titulo'); ?></strong>
                                </a>

                                <div class="content-features">
                                    <div class="tab-pane fade show active" id="ambient" role="tabpanel">
                                        <strong><?= get_field('ambiente_titulo'); ?></strong>
                                        <p><?= get_field('ambiente_descricao'); ?></p>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#security">
                                    <img src="<?= get_field('seguranca_imagem'); ?>" alt="<?= get_field('seguranca_titulo'); ?>" class="img-inactive animated infinite">
                                    <img src="<?= get_field('seguranca_imagem_ativa'); ?>" alt="<?= get_field('seguranca_titulo'); ?>" class="img-active animated infinite">
                                    <strong><?= get_field('seguranca_titulo'); ?></strong>
                                </a>

                                <div class="content-features">
                                    <div class="tab-pane fade show active" id="security" role="tabpanel">
                                        <strong><?= get_field('seguranca_titulo'); ?></strong>
                                        <p><?= get_field('seguranca_descricao'); ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box-gallery">
    <?php
    if (have_posts()) {
        while (have_posts()) {
            the_post();
            the_content();
        }
    }
    ?>
</div>

<div class="section section-awards">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="section-title wow fadeInLeft animated">
                    <h3 class="title text-title">PRÊMIOS</h3>

                    <div class="container-inner">
                        <p class="support text-support" style="color: #00af5d; font-weight: 500; font-size: 20px;">
                            <?= get_field('premio_destaque'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-brasao wow fadeIn animated">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="box-image">
                        <img src="<?= get_field('premio_imagem'); ?>" alt="" />
                    </div>
                </div>
                <div class="col-12 col-md-8">
                    <div class="box-description">
                        <span><?= get_field('premio_ano'); ?></span>
                        <strong><?= get_field('premio_titulo'); ?></strong>
                        <p><?= get_field('premio_descricao'); ?></p>
                    </div>
                </div>
            </div>
        </div>

        <?php
        $args = array(
            'post_type' => 'awards'
        );

        $loop = new WP_Query($args);
        if ($loop->have_posts()) { ?>
            <div class="box-awards wow fadeInUp animated">
                <div class="row">
                    <div class="col-md-12">
                        <div id="carouselAwards" class="carousel slide items-awards" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <?php
                                    while ($loop->have_posts()) {
                                        $loop->the_post(); ?>
                                        <div class="item">
                                            <div class="box-image">
                                                <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                            </div>
                                            <div class="box-info">
                                                <span><?= get_field('premio_ano'); ?></span>
                                                <strong><?php the_title(); ?></strong>
                                                <p>
                                                    <?php the_content(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php
                                } ?>
                                </div>
                                <a class="carousel-control-prev" href="#carouselAwards" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselAwards" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>

                        <div id="carouselAwards" class="carousel slide items-awards mobile" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php
                                while ($loop->have_posts()) {
                                    $loop->the_post(); ?>
                                    <div class="carousel-item">
                                        <div class="item">
                                            <div class="box-image">
                                                <img src="<?php the_post_thumbnail_url(); ?>" alt="">
                                            </div>
                                            <div class="box-info">
                                                <span><?= get_field('premio_ano'); ?></span>
                                                <strong><?php the_title(); ?></strong>
                                                <p>
                                                    <?php the_content(); ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <a class="carousel-control-prev" href="#carouselAwards" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselAwards" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div> 
                        
                        <script>
                            $(function(){
                                $(".carousel.mobile").children(".carousel-item").eq(0).addClass("active");
                            });
                        </script>
                    </div>
                </div>
            </div>
        <?php
        }
    ?>
    </div>
</div>

<?php get_footer(); ?>