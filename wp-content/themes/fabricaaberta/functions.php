<?php

//Thumbnails
add_theme_support('post-thumbnails');

//Title 
function titleSite()
{
    bloginfo('name ');
    if (!is_home()) {
        echo " | ";
    }
    the_title();
}

//Register_Menu
function registerMainMenu()
{
    register_nav_menu('header_menu', 'main-menu');
}
add_action('init', 'registerMainMenu');


//Widget_Header
function widget_header()
{
    register_sidebar(array(
        'name' => 'Widget Header',
        'id' => 'widget_header',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'widget_header');

//Widget Aside
function widget_aside_blog()
{
    register_sidebar(array(
        'name' => 'Widget Aside Blog',
        'id' => 'widget_aside_blog',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'widget_aside_blog');

//PostType Products
function post_type_products(){
    $name = 'Produtos';
    $name_plural = 'Produtos';
    $labels = array(
        'name' => $name,
        'name_plural' => $name_plural,
        'add_new_item' => 'Adicionar novo ' . $name,
        'edit_item' => 'Editar ' . $name
    );
    $supports = array(
        'title',
        'editor',
        'thumbnail'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-admin-post',
        'supports' => $supports
    );
    register_post_type('products', $args );
}
add_action('init', 'post_type_products');

//PostType Awards
function post_type_awards(){
    $name = 'Prêmios';
    $name_plural = 'Prêmios';
    $labels = array(
        'name' => $name,
        'name_plural' => $name_plural,
        'add_new_item' => 'Adicionar novo ' . $name,
        'edit_item' => 'Editar ' . $name
    );
    $supports = array(
        'title',
        'editor',
        'thumbnail'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-admin-post',
        'supports' => $supports
    );
    register_post_type('awards', $args );
}
add_action('init', 'post_type_awards');

//PostType Pagination
function wordpress_pagination()
{
    global $wp_query;

    $big = 999999999;

    echo paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}
