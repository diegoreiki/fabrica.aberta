<?php $home = get_template_directory_uri(); ?>

<!DOCTYPE html>
<html lang="pt-BR" class="js">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

    <title><?php titleSite(); ?></title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600" rel="stylesheet" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/reset.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/helper.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/animate.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/all.min.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/fontawesome.min.css" />
    <link rel="stylesheet" href="<?= $home ?>/assets/css/screen.css" />

    <script type="text/javascript" src="<?= $home ?>/assets/js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="<?= $home ?>/assets/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= $home ?>/assets/js/wow.min.js"></script>
    <script type="text/javascript" src="<?= $home ?>/assets/js/fontawesome.min.js"></script>
    <script type="text/javascript" src="<?= $home ?>/assets/js/scripts.js"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <button class="btn btn-main-menu collapsed d-none" type="button" data-toggle="collapse" data-target="#menuMain">
                        <i class="fa fa-bars d-none"></i>
                        <i class="fa fa-times"></i>
                    </button>                    
                    <div class="box-logotipo">
                        <h1>
                            <a href="<?= get_home_url(); ?>" title="<?php titleSite(); ?>" class="logotipo">
                                <img src="<?= $home ?>/assets/img/logotipo.png" alt="<?php titleSite(); ?>" class="img-fluid" />
                            </a>
                        </h1>
                    </div>
                </div>
                <div class="col-md-10 no-padding flex">
                    <div class="main-menu navbar-collapse collapse" id="menuMain">
                        <?php
                        $args = array(
                            'theme_location' => 'header_menu'
                        );
                        wp_nav_menu($args);
                        ?>
                    </div>
                    <div class="box-language">
                        <a href="#" title="Language">
                            <img src="<?= $home ?>/assets/img/ico-globe.png" alt="<?php titleSite(); ?>" />
                            <img src="<?= $home ?>/assets/img/ico-globe-mobile.png" alt="<?php titleSite(); ?>" class="mobile" />
                        </a>
                        <div class="options-language">
                            <ul>
                                <li>
                                    <a href="#" title="PT" class="active">PT</a>
                                </li>
                                <li>
                                    <a href="#" title="ES">ES</a>
                                </li>
                                <li>
                                    <a href="#" title="EN">EN</a>
                                </li>                                                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>