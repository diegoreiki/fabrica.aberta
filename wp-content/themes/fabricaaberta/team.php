<?php /* Template Name: Nossa Equipe */ ?>

<?php
$home = get_template_directory_uri();
get_header();
?>

<!-- Banner -->
<div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
    <?php the_post_thumbnail(); ?>
</div>
<!-- /Banner -->

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title animated">
                    <h3 class="title text-title"><?= get_field('descricao_titulo'); ?></h3>
                    <p>
                        <strong style="font-size: 20px; color: #00af5d;"><?= get_field('descricao_sub_titulo'); ?></strong>
                    </p>
                    <p class="support text-support">
                        <strong><?= get_field('descricao_chamada'); ?></strong><br />
                        <?= get_field('descricao_conteudo'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="section-page section-team">
    <div class="container">

        <div class="box-videos">
            <div class="main-video wow fadeInLeft animated">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-info">
                            <div class="box-title">
                                <strong>
                                    <?= get_field('video_destaque_1_nome'); ?>
                                    <span><?= get_field('video_destaque_1_cargo'); ?></span>
                                </strong>
                            </div>
                            <div class="box-description">
                                <p>
                                    <?= get_field('video_destaque_1_descricao'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-video">
                            <a href="<?= get_field('video_destaque_1_link'); ?>" title="" class="fancybox-youtube">
                                <img src="<?= get_field('video_destaque_1_thumb'); ?>" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-videos wow fadeInUp animated">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_1_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_1_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_1_cargo'); ?>
                                    <span><?= get_field('video_1_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_1_link'); ?>" title="">
                                    <img src="<?= get_field('video_1_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_1_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_2_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_2_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_2_cargo'); ?>
                                    <span><?= get_field('video_2_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_2_link'); ?>" title="">
                                    <img src="<?= get_field('video_2_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_2_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_3_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_3_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_3_cargo'); ?>
                                    <span><?= get_field('video_3_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_3_link'); ?>" title="">
                                    <img src="<?= get_field('video_3_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_3_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-videos">
            <div class="main-video wow fadeInLeft animated">
                <div class="row">
                    <div class="col-md-6">
                        <div class="box-info">
                            <div class="box-title">
                                <strong>
                                    <?= get_field('video_destaque_2_nome'); ?>
                                    <span><?= get_field('video_destaque_2_cargo'); ?></span>
                                </strong>
                            </div>
                            <div class="box-description">
                                <p>
                                    <?= get_field('video_destaque_2_descricao'); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box-video">
                            <a href="<?= get_field('video_destaque_2_link'); ?>" title="" class="fancybox-youtube">
                                <img src="<?= get_field('video_destaque_2_thumb'); ?>" alt="" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="list-videos wow fadeInUp animated">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_4_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_4_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_4_cargo'); ?>
                                    <span><?= get_field('video_4_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_4_link'); ?>" title="">
                                    <img src="<?= get_field('video_4_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_4_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_5_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_5_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_5_cargo'); ?>
                                    <span><?= get_field('video_5_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_5_link'); ?>" title="">
                                    <img src="<?= get_field('video_5_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_5_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                    <div class="col-md-4">
                        <div class="box-item">
                            <div class="box-image">
                                <img src="<?= get_field('video_6_foto'); ?>" alt="">
                            </div>
                            <div class="box-title">
                                <strong><?= get_field('video_6_nome'); ?></strong>
                                <p>
                                    <?= get_field('video_6_cargo'); ?>
                                    <span><?= get_field('video_6_cidade'); ?></span>
                                </p>
                            </div>
                            <div class="box-video">
                                <a href="<?= get_field('video_6_link'); ?>" title="">
                                    <img src="<?= get_field('video_6_thumb'); ?>" alt="">
                                </a>
                            </div>
                            <p class="description">
                                <?= get_field('video_6_descricao'); ?>
                            </p>
                        </div>
                        <a href="#" class="btn-custom btn-custom-primary view-more d-none">Ver Mais</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_footer(); ?>