<?php /* Template Name: Agendamento de Visita */ ?>

<?php
$home = get_template_directory_uri();
get_header();
?>

<!-- Banner -->
<div class="box-banner" style="background-image: url('<?= get_field('banner_mobile'); ?>');">
    <?php the_post_thumbnail(); ?>
</div>
<!-- /Banner -->

<!-- Section -->
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title animated">
                    <h3 class="title text-title"><?= get_field('titulo'); ?></h3>
                    <p class="support text-support">
                        <?= get_field('descricao'); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-features">
    <div class="container">
        <div class="box-title">
            <strong>Orientações para a visita</strong>
        </div>
        
        <div class="list-features wow fadeInRight animated">
            <div class="row">
                <div class="col-md-3">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_idade_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_idade_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_idade_descricao'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_obrigatorio_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_obrigatorio_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_obrigatorio_descricao'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_proibido_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_proibido_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_proibido_descricao'); ?>
                        </p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_limite_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_limite_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_limite_descricao'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="list-features mobile d-none wow fadeInRight animated animated carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_idade_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_idade_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_idade_descricao'); ?>
                        </p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_obrigatorio_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_obrigatorio_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_obrigatorio_descricao'); ?>
                        </p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_proibido_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_proibido_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_proibido_descricao'); ?>
                        </p>
                    </div>
                </div>

                <div class="carousel-item">
                    <div class="box-image">
                        <img src="<?= get_field('orientacoes_limite_imagem'); ?>" alt="" />
                    </div>
                    <div class="box-description">
                        <strong><?= get_field('orientacoes_limite_titulo'); ?></strong>
                        <p>
                            <?= get_field('orientacoes_limite_descricao'); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-warning">
    <div class="box-banner">
        <img src="<?= $home ?>/assets/img/bg-agendamento.jpg" alt="" class="img-fluid" />
    </div>

    <div class="container">
        <div class="box-warning">
            <strong>Fique atento!</strong>
            <strong class="mobile d-none">Clique e fique atento <i class="fa fa-sort-down"></i></strong>
            <ul>
                <li>
                    <p>
                        <?= get_field('a_tento_dica_1_descricao'); ?>
                    </p>
                </li>
                <li>
                    <p>
                        <?= get_field('a_tento_dica_2_descricao'); ?>
                    </p>
                </li>
                <li>
                    <p>
                        <?= get_field('a_tento_dica_3_descricao'); ?>
                    </p>
                </li>
                <li>
                    <p>
                        <?= get_field('a_tento_dica_4_descricao'); ?>
                    </p>
                </li>
                <li>
                    <p>
                        <?= get_field('a_tento_dica_5_descricao'); ?>
                    </p>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /Section -->

<!-- Section -->
<div class="section section-schedule">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title wow fadeInLeft animated">
                    <h3 class="title text-title">AGENDE SUA VISITA agora!</h3>
                    <p class="support text-support">
                        Para participar do Programa Fábrica Aberta, selecione abaixo qual das nossas unidades deseja visitar.
                    </p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <ul class="nav header-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" role="tab" aria-selected="true" href="#brasil">
                            Brasil
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#argentina">
                            Argentina
                        </a>
                    </li>
                </ul>

                <div class="tab-content content-schedule" id="myTabContent">
                    <div class="tab-pane fade show active" id="brasil" role="tabpanel">

                        <ul class="nav header-city" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="true" href="#santoandre">
                                    Fábrica <span>Santo André</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" role="tab" aria-selected="false" href="#cubatao">
                                    Fábrica <span>Cubatão</span>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content content-city" id="myTabContent">
                            <div class="tab-pane fade" id="santoandre" role="tabpanel">
                                <div class="box-download">
                                    <strong><?= get_field('fabrica_01_titulo'); ?></strong>
                                    <p>
                                        <?= get_field('fabrica_01_descricao'); ?>
                                    </p>
                                    <div class="box-buttons">
                                        <a href="<?= get_field('fabrica_01_link_orientacoes'); ?>" target="_blank" title="Orientações para a visita">Orientações para a visita <span><i class="fa fa-download"></i></span></a>
                                        <a href="<?= get_field('fabrica_01_link_termo'); ?>" target="_blank" title="Termo de reconhecimento">Termo de reconhecimento <span><i class="fa fa-download"></i></span></a>
                                    </div>
                                </div>

                                <div class="box-form">
                                    <?= get_field('fabrica_01_formulario'); ?>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="cubatao" role="tabpanel">
                                <div class="box-download">
                                    <strong><?= get_field('fabrica_02_titulo'); ?></strong>
                                    <p>
                                        <?= get_field('fabrica_02_descricao'); ?>
                                    </p>
                                    <div class="box-buttons">
                                        <a href="<?= get_field('fabrica_02_link_orientacoes'); ?>" target="_blank" title="Orientações para a visita">Orientações para a visita <span><i class="fa fa-download"></i></span></a>
                                        <a href="<?= get_field('fabrica_02_link_termo'); ?>" target="_blank" title="Termo de reconhecimento">Termo de reconhecimento <span><i class="fa fa-download"></i></span></a>
                                    </div>
                                </div>

                                <div class="box-form">
                                    <?= get_field('fabrica_02_formulario'); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="argentina" role="tabpanel">

                        <div class="tab-content content-city" id="myTabContent" style="display: block !important;">

                            <div class="tab-pane fade show active" id="bahiablanca" role="tabpanel">
                                <div class="box-download">
                                    <strong><?= get_field('fabrica_03_titulo'); ?></strong>
                                    <p>
                                        <?= get_field('fabrica_03_descricao'); ?>
                                    </p>
                                    <div class="box-buttons">
                                        <a href="<?= get_field('fabrica_03_link_orientacoes'); ?>" target="_blank" title="Orientações para a visita">Orientações para a visita <span><i class="fa fa-download"></i></span></a>
                                        <a href="<?= get_field('fabrica_03_link_termo'); ?>" target="_blank" title="Termo de reconhecimento">Termo de reconhecimento <span><i class="fa fa-download"></i></span></a>
                                    </div>
                                </div>

                                <div class="box-form">
                                    <?= get_field('fabrica_03_formulario'); ?>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Section -->

<?php get_footer(); ?>